<?php

namespace Tests\Feature;

use App\Link;
use Tests\TestCase;
use Tests\DuskTestCase;
use Laravel\Dusk\Chrome;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;

class LinksTest extends DuskTestCase
{
    // TODO: Find a way to set in config files or to be auto-found
    protected $baseURL = "http://127.0.0.1:8000/";
    private $title = "My Title";

    public function testVisitHome_shouldShowCreatedLinkTitle()
    {
        factory(\App\Link::class)->create([
            'title' => $this->title,
        ]);
        // Laravel 5.4 Dusk browser testing
        $this->browse(function ($browser) {
            $browser->visit($this->baseURL)
                ->assertSee($this->title);
        });
    }

    public function testVisitSubmitPage_shouldShowSubmitLink()
    {
        $this->browse(function ($browser) {
            $browser->visit($this->baseURL.'submit')
                ->assertSee("Submit a link");
        });
    }

    public function testSubmitEmptySubmitForm_shouldShowValidationErrors()
    {
        $this->browse(function ($browser) {
            $browser->visit($this->baseURL.'submit')
                ->press('Submit')
                ->assertSee('The title field is required')
                ->assertSee('The url field is required')
                ->assertSee('The description field is required');
        });
    }

    public function testSubmitOKSubmitForm_shouldSaveInDatabase()
    {
        $this->browse(function ($browser) {
            $browser->visit($this->baseURL.'submit')
                ->type('title', $this->title)
                // URLs are unique
                ->type('url', 'https://'.date('His').'.com')
                ->type('description', 'My sample description')
                ->press('Submit');
            $this->assertDatabaseHas('links', ['title' => $this->title]);
        });
    }
}
