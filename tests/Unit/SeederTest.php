<?php

namespace Tests\Unit;

use App\Link;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SeederTest extends TestCase
{
    /**
     *
     * @return void
     */
    public function testLinksTable_shouldNotContainTitle()
    {
        $title = "My Title";
        factory(\App\Link::class)->create([
            'title' => $title,
        ]);
        $this->assertDatabaseHas('links', ['title' => $title]);
    }
}
