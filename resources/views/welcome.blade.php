<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- TODO: declare website title in constant -->
        <title>Laravel</title>

    </head>
    <body>
        <div>
            <ul>
            @foreach ($links as $link)
                <li>{{ $link }}</li>
            @endforeach
            </ul>
        </div>
    </body>
</html>
